# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# open-source-office@arm.com
#
# SPDX-License-Identifier: MIT

SUMMARY = "JFrog command line client application"
HOMEPAGE = "https://github.com/jfrog/jfrog-cli"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=dc7f21ccff0f672f2a7cd6f412ae627d"

GO_IMPORT = "github.com/jfrog/jfrog-cli"
SRC_URI = "git://${GO_IMPORT};branch=v2;protocol=https;destsuffix=${BPN}-${PV}/src/${GO_IMPORT}"

# Points to 2.20.0 tag
SRCREV = "ca9d81ce27a519cb38898d73be1d94d207e5ecf6"

inherit go-mod

do_compile[network] = "1"

GO_INSTALL = "${GO_IMPORT}"
GO_LINKSHARED = ""

RDEPENDS:${PN}-dev += "bash make"
