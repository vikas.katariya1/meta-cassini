# Copyright (c) 2022-2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
---
.test-preparation:
  extends: .resource-request
  stage: Test
  image: $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG
  variables:
    DOCKER_IMAGE_NAME: lava-test-image
    KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 5Gi
  rules:
    - if: $LAVA_URL
  script:
    - mkdir cassini-firmware
    - pushd cassini-firmware
    - joburl=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}
    - datafile=${CI_PROJECT_DIR}/build_data.env
    - |
      case "${MACHINE}" in
        'n1sdp')
          echo "Building SD Card image for N1SDP"
          fwfile=firmware.zip
          # Update the PMIC_FORCE parameter in the config.txt file in the
          # firmware package to TRUE.
          # Setting it to FALSE will require user input so the boot process
          # halts.
          fwtar=${FW_IMAGE}-board-firmware_primary.tar.gz
          tar -zxvf ${CI_PROJECT_DIR}/${ARTIFACT_DIR}/${fwtar}
          config="PMIC_FORCE:"
          current_value=" FALSE"
          new_value=" TRUE"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          config="MCPUART:"
          current_value="   1"
          new_value="   0"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          config="FPGAUART2:"
          current_value=" 0"
          new_value=" 1"
          sed -i "s/$config$current_value/$config$new_value/" config.txt
          zip -r ${CI_PROJECT_DIR}/${fwfile} *
          echo "FIRMWARE_ARTIFACT_URL=${joburl}/artifacts/${fwfile}" \
            >> ${datafile}
          ;;
        'corstone1000-mps3')
          echo "Building images for Corstone-1000 for MPS3"
          fwfile=firmware.zip
          git clone --branch corstone1k \
            https://${OSS_GERRIT_SERVER}/iot-sw/corstone1k/fpga_bitfiles
          export \
          BUILD_DIR=${CI_PROJECT_DIR}/${ARTIFACT_DIR}
          export WIC_NAME="${FW_IMAGE}-${MACHINE}.wic"
          # Create restore image directory structure
          mkdir --parents cs1k-restore/SOFTWARE cs1k-restore/MB
          # Copy required files to correct place
          cp ${BUILD_DIR}/${WIC_NAME} \
            cs1k-restore/SOFTWARE/cs1000.bin
          cp ${BUILD_DIR}/${BL1_IMAGE} \
            cs1k-restore/SOFTWARE/bl1.bin
          cp ${BUILD_DIR}/${EXTSYS_IMAGE} \
            cs1k-restore/SOFTWARE/es0.bin
          cp -r fpga_bitfiles/Boardfiles/MB/* \
            cs1k-restore/MB/
          cp fpga_bitfiles/Boardfiles/config.txt \
            cs1k-restore/config.txt
          # Generate new configs
          find cs1k-restore/MB -type f -name 'images.txt' | xargs \
            sed -i -e "s/^TOTALIMAGES:.*/TOTALIMAGES: 3/g" \
              -e "s/^IMAGE2.*//g" \
              -e "s/^IMAGE0FILE:.*/IMAGE0FILE: \\\\SOFTWARE\\\\bl1.bin/g" \
              -e "s/^IMAGE1FILE:.*/IMAGE1FILE: \\\\SOFTWARE\\\\es0.bin/g" \
              -e "s/^IMAGE3\(.*\)/IMAGE2\1/g" \
              -e "s/^IMAGE2FILE:.*/IMAGE2FILE: \\\\SOFTWARE\\\\cs1000.bin/g"
          find cs1k-restore/MB -type f -name 'images.txt' | xargs cat
          # Now zip the new fpga bitfile Set
          pushd cs1k-restore
          zip -r ${CI_PROJECT_DIR}/${fwfile} *
          popd  # cs1k-restore
          echo "FIRMWARE_ARTIFACT_URL=${joburl}/artifacts/${fwfile}" \
            >> ${datafile}
          ;;
        'corstone1000-fvp')
          echo "Nothing to be done for Corstone-1000 FVP"
          echo "FIRMWARE_ARTIFACT_URL=${FW_IMAGE_ARTIFACT_URL}" >> ${datafile}
          ;;
          *)
          echo "Unkown machine ${MACHINE}"
          exit 1
          ;;
      esac
    - popd  # cassini-firmware
    - cat ${CI_PROJECT_DIR}/build_data.env
  artifacts:
    paths:
      - "build_data.env"
      - "firmware.zip"
      - "**/*.fvpconf"
    reports:
      dotenv: build_data.env
    expire_in: "1 days"

.submit-cassini-lava-job:
  extends: .submit-lava-job
  variables:
    FIRMWARE_ARTIFACT: ${FIRMWARE_ARTIFACT_URL}
    BMAP_ARTIFACT: ${BMAP_ARTIFACT_URL}
    IMAGE_ARTIFACT: ${IMAGE_ARTIFACT_URL}
    UTIL_ARTIFACT: ${UTIL_IMAGE_ARTIFACT_URL}
    BL1_ARTIFACT: ${BL1_IMAGE_ARTIFACT_URL}
    EXTSYS_ARTIFACT: ${EXTSYS_IMAGE_ARTIFACT_URL}
    LAVA_JOB_TEMPLATE: .gitlab/lava/$MACHINE/$LAVA_JOB.j2
  script:
    # Fill in job template with image locations
    - |
      if [ ${MACHINE} == 'corstone1000-fvp' ]; then
        j2 ${LAVA_JOB_TEMPLATE} -o ${LAVA_JOB}.tmp
        pushd ${ARTIFACT_DIR}
        FVP_CONFIG_FILE=$(readlink ${FVP_CONFIG}-${MACHINE}.fvpconf)
        popd
        python3 .gitlab/scripts/fvpconf-to-lava.py \
          "${ARTIFACT_DIR}/${FVP_CONFIG_FILE}" \
          "${LAVA_JOB}.tmp" \
          "${LAVA_JOB}.tmp.j2"
        export LAVA_JOB_TEMPLATE="${LAVA_JOB}.tmp.j2"
      fi
    - !reference [".submit-lava-job", script]
  artifacts:
    paths:
      - test_data.env
      - $LAVA_JOB.tmp
      - $LAVA_JOB.tmp.j2
    reports:
      dotenv: test_data.env
    expire_in: 1 day

.ptest-tests:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: ptest.yml

.ptest-tests-results:
  extends: .complete-lava-job
  after_script:
    - python3 .gitlab/scripts/lava-to-junit.py
      lava-result.yml
      "TEST-${CI_JOB_NAME}-test-report.xml"
    - |
      jfrog config add artifactory-aws --interactive=false \
      --artifactory-url=$ARTIFACTORY_AWS_URL --user=$ARTIFACTORY_USER \
      --password=$ARTIFACTORY_PASS;
    - echo LAUNCH_CI_JOB_NAME_AND_ID="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - jobinfo="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - buildid="oss-cassini/${CI_PROJECT_PATH}/${jobinfo}"
    - |
      cat << EOF > ./download_spec.json
      {
        "files": [
          {
            "pattern": "oss-cassini.lava-images-temp/ptest-runner-results/",
            "build": "${buildid}",
            "flat": "false",
            "recursive": "true"
          }
        ]
      }
      EOF
    - cat download_spec.json
    - jfrog rt download --spec=download_spec.json --fail-no-op
    - |
      python3 .gitlab/scripts/ptest-to-junit.py \
      ptest-runner-results \
      TEST-ptest-runner-test-report.xml
  artifacts:
    paths:
      - ptest-runner-results/${LAUNCH_CI_JOB_ID}/**
      - TEST-*-test-report.xml
      - lava-result.yml
    expire_in: 1 day
    reports:
      junit:
        - TEST-*-test-report.xml
    when: always


.acs-test:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: acs_suite.yml
    IR_IMG: "ACS/ir_acs_live_image-${MACHINE}.img"
    BMAP_ARTIFACT: "$ARTIFACTORY_URL/oss-cassini.lava-images-temp/$IR_IMG.bmap"
    IMAGE_ARTIFACT: "$ARTIFACTORY_URL/oss-cassini.lava-images-temp/$IR_IMG.bz2"

.acs-test-results:
  extends: .complete-lava-job
  after_script:
    - python3 .gitlab/scripts/lava-to-junit.py
      lava-result.yml
      "TEST-${CI_JOB_NAME}-test-report.xml"
    - |
      jfrog config add artifactory-aws --interactive=false \
      --artifactory-url=$ARTIFACTORY_AWS_URL --user=$ARTIFACTORY_USER \
      --password=$ARTIFACTORY_PASS;
    - echo LAUNCH_CI_JOB_NAME_AND_ID="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - jobinfo="${LAUNCH_CI_JOB_NAME}/${LAUNCH_CI_JOB_ID}"
    - buildid="oss-cassini/${CI_PROJECT_PATH}/${jobinfo}"
    - echo SEQUENCE_FILE=$SEQUENCE_FILE
    - |
      cat << EOF > ./download_spec.json
      {
        "files": [
          {
            "pattern": "oss-cassini.lava-images-temp/acs_results/",
            "build": "${buildid}",
            "flat": "false",
            "recursive": "true"
          }
        ]
      }
      EOF
    - cat download_spec.json
    - jfrog rt download --spec=download_spec.json --fail-no-op
    - python3  .gitlab/scripts/BSA-to-junit.py
      ./acs_results/uefi/BsaResults.log
      TEST-BSA-test-report.xml
    - python3  .gitlab/scripts/fwts-to-junit.py
      ./acs_results/fwts/FWTSResults.log
      TEST-FWTS-test-report.xml
    - python3 /builder/SCT_Parser/parser.py
      ./acs_results/sct_results/Overall/Summary.ekl
      ./acs_results/sct_results/Sequence/EBBR.seq
      --junit TEST-SCT-${SEQUENCE_FILE}-test-report.xml
  artifacts:
    paths:
      - lava-console.log
      - acs_results/**
      - TEST-*-test-report.xml
      - lava-result.yml
    expire_in: 1 week

.sanity-test:
  extends: .submit-cassini-lava-job
  variables:
    LAVA_JOB: sanity_job.yml

.sanity-test-results:
  extends: .complete-lava-job
  artifacts:
    paths:
      - lava-result.yml
    expire_in: 1 day
    reports:
      junit:
        - TEST-*-test-report.xml
    when: always
