# Copyright (c) 2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

# Generate the cassini wic image
IMAGE_FSTYPES += "wic wic.gz wic.bmap"

# Remove initramfs image generation for cassini distribution due to size restrictions
INITRAMFS_IMAGE:remove:libc-glibc =  "corstone1000-initramfs-image"

# Extend the wks search path to find custom wks file
WKS_SEARCH_PATH:prepend:libc-glibc:corstone1000 := "${CASSINI_ARM_BSP_DYNAMIC_DIR}/wic:"
WKS_FILE:libc-glibc:corstone1000 = "corstone1000-efidisk.wks.in"
GRUB_CFG_FILE:libc-glibc:corstone1000 ?= "${CASSINI_ARM_BSP_DYNAMIC_DIR}/wic/corstone1000-grub.cfg"

# Add grub-efi
EFI_PROVIDER:libc-glibc ?= "grub-efi"
MACHINE_FEATURES:append:libc-glibc = " efi"
