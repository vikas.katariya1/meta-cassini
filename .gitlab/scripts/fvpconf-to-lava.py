#!/usr/bin/env python3
# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT
import argparse
import json
import re
import sys
import yaml


def main():
    parser = argparse.ArgumentParser(
        description='Adds FVP config options to lava test files')

    parser.add_argument("fvpconf", help="Yocto build generated fvpconf file")
    parser.add_argument("lavafile", help="Input lava definition file")
    parser.add_argument("lavaout", default="lava_definition.yml",
                        help="Output lava definition file")

    args = parser.parse_args()

    with open(args.fvpconf, "r") as fvpconf_file:
        fvpconf = json.load(fvpconf_file)

        PatchLavaFile(fvpconf, args.lavafile, args.lavaout)


def PatchLavaFile(fvpconf, lavafile, lavaout):
    with open(lavafile) as lavafile:
        lavadata = yaml.safe_load(lavafile)

        # Update the fvpconf with parts of lava script that need retaining
        for action in lavadata['actions']:
            if "boot" in action and action['boot']['method'] == "fvp":
                fvpconf = ParseArgs(fvpconf, action['boot']['arguments'])

        newArguments = []
        for arg, data in fvpconf['parameters'].items():
            newArguments.append("-C " + arg + "=\"" + data + "\"")

        for data in fvpconf['data']:
            newArguments.append("--data " + data)

        # Now update the lava file data with the fvpconf
        for i in range(len(lavadata['actions'])):
            if ("boot" in lavadata['actions'][i] and
                    lavadata['actions'][i]['boot']['method'] == "fvp"):
                lavadata['actions'][i]['boot']['arguments'] = newArguments

        # And finally output the new file
        with open(lavaout, 'w') as lavaout:
            yaml.dump(lavadata, lavaout,
                      default_flow_style=False)


def ParseArgs(fvpconf, lavaargs):
    cargs = {}
    data = {}

    finddata = re.compile(r'^(-(C|-data))\s+(.+)?=(.+)$')
    findreplacement = re.compile(r'^.*{[0-9A-Z_]+}.*$')
    for arg in lavaargs:
        match = finddata.match(arg)

        argtype = match.group(1)
        argname = match.group(3)
        argdata = match.group(4)

        if findreplacement.match(argdata):
            if argtype == "-C":
                cargs[argname] = argdata
            elif argtype == "--data":
                data[argname + "="] = argdata
            else:
                print("Unnknown arg type found: " + argtype)

    # parameters is a dictionary
    fvpconf['parameters'].update(cargs)
    # data is a list of strings
    for i in range(len(fvpconf['data'])):
        if fvpconf['data'][i].startswith(tuple(data.keys())):
            elementname = fvpconf['data'][i].split('=')[0] + "="
            # Need to sort out the load address if one exists
            # as we want the version from the fvpconf file
            # not the origional in the lava file
            loadaddress = fvpconf['data'][i].split('@')[1]
            if loadaddress != "":
                loadaddress = "@" + loadaddress
            fvpconf['data'][i] = elementname + \
                data[elementname].split('@')[0] + loadaddress

    return fvpconf


if __name__ == '__main__':
    main()
