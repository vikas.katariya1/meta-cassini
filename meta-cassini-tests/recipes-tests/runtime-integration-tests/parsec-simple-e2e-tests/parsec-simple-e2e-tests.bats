#!/usr/bin/env bats
#
# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

# Run-time validation tests for PARSEC service in Cassini stack.

# Set generic configuration

if [ -z "${PS_TEST_LOG_DIR}" ]; then
    TEST_LOG_DIR="${HOME}/runtime-integration-tests-logs"
else
    TEST_LOG_DIR="${PS_TEST_LOG_DIR}"
fi

export TEST_LOG_FILE="${TEST_LOG_DIR}/parsec-simple-e2e-tests.log"
export TEST_STDERR_FILE="${TEST_LOG_DIR}/ps-stderr.log"
export TEST_RUN_FILE="${TEST_RUNTIME_DIR}/parsec-simple-e2e-tests.pgid"
export PARSEC_TOOL="/usr/bin/parsec-tool -t 3600"

export TEST_CLEAN_ENV="${PS_TEST_CLEAN_ENV:=1}"

load "${TEST_COMMON_DIR}/integration-tests-common-funcs.sh"

# There are no base clean-up activities required
# Function is defined and called so that it can be conditionally overridden
clean_test_environment() {
  :
}

@test 'List all providers for parsec-service' {

    _run /usr/bin/parsec-tool list-providers
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}

@test 'Check if Core provider is enabled for parsec-service' {

    _run /usr/bin/parsec-tool list-providers
    if echo "${output}" | grep "ID: 0x00 (Core provider)" ; then
        log "PASS"
    else
        log "FAIL"
        return 1
    fi
}

@test 'Check if trusted-service provider is enabled for parsec-service' {

    _run /usr/bin/parsec-tool list-providers
    if echo "${output}" | grep "ID: 0x04 (Trusted Service provider)" ; then
        log "PASS"
    else
        log "FAIL"
        return 1
    fi
}

@test 'Check if Mbed-crypto provider is disabled for parsec-service' {

    _run /usr/bin/parsec-tool list-providers
    if echo "${output}" | grep "ID: 0x01 (Mbed Crypto provider)" ; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}

@test 'simple end to end tests for PARSEC service' {
    
    # PARSEC_TOOL is used by parsec-cli-tests.sh internally
    _run /usr/bin/parsec-cli-tests.sh
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
