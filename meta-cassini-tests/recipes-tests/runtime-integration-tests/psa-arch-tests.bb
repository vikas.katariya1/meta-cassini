# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

SUMMARY = "psa-arch-tests (aka psa api tests)"
DESCRIPTION = "psa-arch-tests is a test suite to tests different services  \
               provided by Trusted Services. These tests are run from linux \
               userspace. Please refer to https://github.com/ARM-software/psa-arch-tests \
               for more information. "

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = " \
    file://psa-arch-tests.bats \
    "

inherit runtime-integration-tests
require runtime-integration-tests.inc
