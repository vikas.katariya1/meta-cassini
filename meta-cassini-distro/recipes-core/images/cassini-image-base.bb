# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

SUMMARY = "CASSINI base image core config with common packages"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit cassini-image
