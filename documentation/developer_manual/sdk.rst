..
 # Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
 #
 # SPDX-License-Identifier: MIT

##############################
Software Development Kit (SDK)
##############################

Cassini SDK distribution images enable users to perform common development
tasks on the target, such as:

  * Application and kernel module compilation
  * Remote debugging
  * Profiling
  * Tracing
  * Runtime package management

The precise list of packages and image features provided as part of the Cassini
SDK can be found in
``meta-cassini-distro/conf/distro/include/cassini-sdk.inc``.

The Yocto project provides guidance for some of these common development tasks,
for example |kernel module compilation|_, |profiling and tracing|_, and
|runtime package management|_.

See :ref:`software_development_kit_label` for details on including the SDK on
a Cassini distribution image.
