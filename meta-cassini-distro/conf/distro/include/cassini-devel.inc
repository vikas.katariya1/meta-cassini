# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# config specifc to the cassini-devel distro feature, enabled using
# DISTRO_FEATURES

EXTRA_IMAGE_FEATURES:append = " debug-tweaks"
